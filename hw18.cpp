#include <iostream>
#include <cstdlib>

class Stack
{
private:
    int size;
    int* arr;

public:
    Stack()
    {
        size = 0;
        arr = new int[size];
    }
    void pop()
    {
        size--;

    }
    void push(int num)
    {
        size++;
        arr[size] = num;
    }
    int showTop()
    {
        return arr[size];
    }

};

int main()
{
    Stack intStack;

    intStack.push(7);
    intStack.push(5);
    intStack.push(33);
    intStack.pop();
    intStack.pop();
    std::cout << "On top is " << intStack.showTop() << "\n";

}